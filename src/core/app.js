export default function fizzbuzz(number) {
	let message = ""
  if (number % 5 === 0 && number % 3 === 0) {
    message = "fizzbuzz";
  } else if (number % 5 == 0) {
    message = "buzz";
  } else if (number % 3 == 0) {
    message = "fizz";
  }
	return message
}
