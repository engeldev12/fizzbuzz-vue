const express = require('express')
const serverStatic = require('serve-static')


const app = express()

app.use(serverStatic(__dirname + "/dist"))

const PORT = process.env.PORT || 8090

app.listen(PORT, (req, resp) => {
	console.log("Ejectutando servidor en el puerto: " + PORT)
})