import { screen, render } from "@testing-library/vue";
import "@testing-library/jest-dom";

import FizzBuzz from "@/components/FizzBuzz";

describe("FizzBuzz.vue", () => {
  describe("cuando el número es multiplo de 3", () => {
    it("debería dar fizz 3", async () => {
      render(FizzBuzz, {
        props: {
          number: 3,
        },
      });

      await screen.getByRole("button").click();

      expect(screen.getByText(/fizz/i)).toBeInTheDocument();
    });

    it("debería dar fizz 9", async () => {
      render(FizzBuzz, {
        props: {
          number: 9,
        },
      });

      await screen.getByRole("button").click();

      expect(screen.getByText(/fizz/i)).toBeInTheDocument();
    });
  });

  describe("cuando el número es multiplo de 5", () => {
    it("debería dar buzz 5", async () => {
      render(FizzBuzz, {
        props: {
          number: 5,
        },
      });

      await screen.getByRole("button").click();

      expect(screen.getByText(/buzz/i)).toBeInTheDocument();
    });

    it("debería dar fizz 10", async () => {
      render(FizzBuzz, {
        props: {
          number: 10,
        },
      });

      await screen.getByRole("button").click();

      expect(screen.getByText(/buzz/i)).toBeInTheDocument();
    });
  });

  describe("cuando el número es multiplo de 3 y 5", () => {
    it("debería dar fizzbuzz 15", async () => {
      render(FizzBuzz, {
        props: {
          number: 15,
        },
      });

      await screen.getByRole("button").click();

      expect(screen.getByText(/fizzbuzz/i)).toBeInTheDocument();
    });

    it.todo("debería dar fizzbuzz 30")
  });
});
